/*****************************************************
 * Lib sources and docs

// https://learn.adafruit.com/ahrs-for-adafruits-9-dof-10-dof-breakout/software
// Libs
// - LSM9DS0 accel/gyro/mag
// https://github.com/adafruit/Adafruit_LSM9DS0_Library
// https://github.com/adafruit/Adafruit_Sensor
// https://github.com/adafruit/Adafruit_AHRS
// - Adafruit SSD1306 128x64 display / Diymall 0.96" Inch Yellow and Blue I2c IIC Serial 128x64 Oled LCD Oled LED 
// https://github.com/adafruit/Adafruit_SSD1306
// https://github.com/adafruit/Adafruit-GFX-Library
// - sensor smoothing
// http://playground.arduino.cc/Main/DigitalSmooth
// - bluetooth le
// https://github.com/adafruit/Adafruit_BluefruitLE_nRF51
// ble example https://learn.adafruit.com/bluefruit-le-micro-atmega32u4-microcontroller-usb-bluetooth-le-in-one/bleuart use command mode example
// - json
// https://github.com/bblanchon/ArduinoJson
//
// board setup
// Open tools > boards > board manager
// First up, install the Arduino SAMD Boards version 1.6.2 or later (32-bits arm cortex M0+)
// Then install the adafruit SAMD boards
********************************************************/
#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <stdlib.h>
#include <Adafruit_LSM9DS0.h>
#include <Adafruit_Simple_AHRS.h>
//#include <Adafruit_GFX.h>
//#include <Adafruit_SSD1306.h>
//#include <ArduinoJson.h>
#include <Adafruit_Sensor.h>
#include <ResponsiveAnalogRead.h>

// bluetooth libs
#if not defined (_VARIANT_ARDUINO_DUE_X_) && not defined (_VARIANT_ARDUINO_ZERO_)
  #include <SoftwareSerial.h>
#endif
#include "Adafruit_BLE.h"
#include "Adafruit_BluefruitLE_SPI.h"
#include "Adafruit_BluefruitLE_UART.h"
#include "Adafruit_BLEGatt.h"
#include "BluefruitConfig.h"

/***********************************************************
************************************************************
************************************************************
*********
*********
*********     End includes / Begin Configs
*********
*********
************************************************************
************************************************************
***********************************************************/

/********************************
 * Sensor data storage configs
 */
unsigned const int SENSOR_COUNT = 8;
unsigned const int SENSOR_CHAR_BUF_SIZE = 2;
unsigned const int BUF_LEN = 14;
enum sensor {
    pitch,
    roll,
    yaw,
    thumb_fsr,
    index_fsr,
    middle_fsr,
    ring_fsr,
    little_fsr
};
int16_t sensors[SENSOR_COUNT] = {0};
/*
 * End Sensor data storage configs
 ***********************************/
 
/********************************
 * Bluetooth setup
 */
#define MINIMUM_FIRMWARE_VERSION    "0.7.0"
#define MODE_LED_BEHAVIOUR          "MODE"
Adafruit_BluefruitLE_SPI ble(BLUEFRUIT_SPI_CS, BLUEFRUIT_SPI_IRQ, BLUEFRUIT_SPI_RST);
Adafruit_BLEGatt gatt(ble);
// A small helper
void error(const __FlashStringHelper*err) {
  Serial.println(err);
  while (1);
}
// The service and characteristic index_fsr information 
int32_t gattServiceId;
int32_t gattNotifiableCharId;
int32_t gattReadableCharId;
/*
 * End Bluetooth setup
 ***********************************/

/********************************
 * Flora 9DOF config
 */
// Create LSM9DS0 board instance.
Adafruit_LSM9DS0     lsm(1000);  // Use I2C, ID #1000

// Create simple AHRS algorithm using the LSM9DS0 instance's accelerometer and magnetometer.
Adafruit_Simple_AHRS ahrs(&lsm.getAccel(), &lsm.getMag());

// Function to configure the sensors on the LSM9DS0 board.
// You don't need to change anything here, but have the option to select different
// range and gain values.
void configureLSM9DS0(void)
{
  // Set the accelerometer range
  lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_2G);
  // et the magnetometer sensitivity
  lsm.setupMag(lsm.LSM9DS0_MAGGAIN_2GAUSS);
  // Setup the gyroscope
  lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_245DPS);
}
/*
 * Flora 9DOF config
 ***********************************/

/*****************
 * Analog pins
 */
const int NULL_PIN   = A5;
const int THUMB_PIN  = A1;
const int INDEX_PIN  = A2;
const int MIDDLE_PIN = A3;
const int RING_PIN   = A0;
const int LITTLE_PIN = A4;
/*
 * 
 **********************************/
 
/**************
 * sensor smoothing histories
 * Add a hisory for each value that needs smoothing
 */


 
#define filterSamples   15
// FSR histories
ResponsiveAnalogRead thumbf(NULL_PIN, true, 0.01, true);
ResponsiveAnalogRead indexf(NULL_PIN, true, 0.01, true);
ResponsiveAnalogRead middlef(NULL_PIN, true, 0.01, true);
ResponsiveAnalogRead ringf(NULL_PIN, true, 0.01, true);
ResponsiveAnalogRead littlef(NULL_PIN, true, 0.01, true);
ResponsiveAnalogRead ahrs_pitch(NULL_PIN, true, 0.01, true);
ResponsiveAnalogRead ahrs_roll(NULL_PIN, true, 0.01, true);
ResponsiveAnalogRead ahrs_yaw(NULL_PIN, true, 0.01, true);
/*
int16_t thumbHistory[filterSamples];
int16_t indexHistory[filterSamples];
int16_t middleHistory[filterSamples];
int16_t ringHistory[filterSamples];
int16_t littleHistory[filterSamples];
// Position sensor histories
int16_t pitchHistory[filterSamples];
int16_t rollHistory[filterSamples];
int16_t yawHistory[filterSamples];*/
/*
 * End sensor smoothing init
 *******************/

/*****************
 * Logging config
 * 
 *  true = logging on
 *  false = logging off
 */
const bool SERIAL_LOGGING = false;
/*
 * End logging config
 *******************/

/***********************************************************
************************************************************
************************************************************
*********
*********
*********     End config section / Begin setup function
*********
*********
************************************************************
************************************************************
***********************************************************/

void setup(void) 
{ 
  /**************
  * Init serial
  */
  Serial.begin(9600);
  Serial.write("Serial initialized");
  /*
  * End serial init
  *******************/

  /**************
  * init bluetooth
  */
  if ( !ble.begin(VERBOSE_MODE) )
  {
    error(F("Couldn't find Bluefruit, make sure it's in CoMmanD mode & check wiring?"));
  }
  Serial.println( F("OK!") );
  // Disable command echo from Bluefruit
  ble.echo(false);
  // Print Bluefruit information
  Serial.println(F("Requesting Bluefruit info:"));
  ble.info();
  ble.verbose(false);
  /*
  * End bluetooth init
  *******************/

  /**************
  * Start fresh with a factory reset
  * This is important for clearing out custom ble services and characteristics. 
  * Otherwise every reset will add new ones with incrementing ids.
  */
  Serial.println(F("Performing a factory reset: "));
  if (! ble.factoryReset() ){
    error(F("Couldn't factory reset"));
  }
  /* 
  * End factory reset
  *******************/

  /**************
  * Configure custom BLE service and characteristics
  * base UUID as defined by Bluetooth SIG: 0000XXXX-0000-1000-8000-00805F9B34FB
  * replace XXXX with the 16bit short uuid to make a complete uuid
  * 16bit uuids are not a real thing. devices will need to look for the 128 uuid
  * our short 16 uuids:
  * 0xFFF0
  * 0xFFF1
  * 
  * our complete 128 uuids:
  * 0000FFF0-0000-1000-8000-00805F9B34FB
  * 0000FFF1-0000-1000-8000-00805F9B34FB
  */
  gattServiceId = gatt.addService(0xFFF0);
  gattNotifiableCharId = gatt.addCharacteristic(0xFFF1, 0x10, 1, 20, BLE_DATATYPE_BYTEARRAY, "customnotifyJF");
  uint8_t advdata[] { 0x02, 0x01, 0x06, 0x05, 0x02, 0xF0, 0xFF, 0x0a, 0x18 };
  ble.setAdvData( advdata, sizeof(advdata) );
  

  
  // Reset the device for the new service setting changes to take effect
  Serial.print(F("Performing a SW reset (service changes require a reset): "));
  ble.println( F("AT+GAPINTERVALS=5,13,20,400"));
  ble.reset();
  ble.println( F("AT+GAPINTERVALS=5,13,20,400"));
  Serial.print(F("SW reset complete"));
  /*
  * End custom BLE service and characteristics
  ********************/

  /**************
  * Init display
  *
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3D (for the 128x64)
  //display.display();
  //delay(2000);
  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  *
  * End display init
  *******************/

  /**************
  * Init LSM9DS0 9 DOF board
  */
  if(!lsm.begin())
  {
    // There was a problem detecting the LSM9DS0 ... check your connections
    Serial.print(F("Ooops, no LSM9DS0 detected ... Check your wiring or I2C ADDR!"));
    while(1);
  } 
  // Setup the sensor gain and integration time.
  configureLSM9DS0();
  /*
  * End LSM9DS0 init
  *******************/

  /*********************
   * wait for BLE connection
   */
   //display.setCursor(0,0);
   //display.println("Waiting for BLE...");
   //Serial.println(F("Waiting for BLE..."));
   //display.display();
   /*while (! ble.isConnected()) {
      delay(500);
   }*/
   //display.clearDisplay();
   //display.display();
   //Serial.println( F("Switching to DATA mode!") );
   //ble.setMode(BLUEFRUIT_MODE_DATA);
   /*  
   * End BLE connect
   **********************/
}

/***********************************************************
************************************************************
************************************************************
*********
*********
*********     End setup function / Begin main loop
*********
*********
************************************************************
************************************************************
***********************************************************/

void loop(void) 
{
  // Orientation vector to fill with euler angles
  sensors_vec_t   orientation;

  // Read the state of the fsrs
  thumbf.update(analogRead(THUMB_PIN));
  indexf.update(analogRead(INDEX_PIN));
  middlef.update(analogRead(MIDDLE_PIN));
  ringf.update(analogRead(RING_PIN));
  littlef.update(analogRead(LITTLE_PIN));

  sensors[thumb_fsr]  = (int16_t)thumbf.getValue();
  sensors[index_fsr]  = (int16_t)indexf.getValue();
  sensors[middle_fsr] = (int16_t)middlef.getValue();
  sensors[ring_fsr]   = (int16_t)ringf.getValue();
  sensors[little_fsr] = (int16_t)littlef.getValue();
  
  /*
  sensors[thumb_fsr]  = digitalSmooth((int16_t)analogRead(THUMB_PIN),  thumbHistory);
  sensors[index_fsr]  = digitalSmooth((int16_t)analogRead(INDEX_PIN),  indexHistory);
  sensors[middle_fsr] = digitalSmooth((int16_t)analogRead(MIDDLE_PIN), middleHistory);
  sensors[ring_fsr]   = digitalSmooth((int16_t)analogRead(RING_PIN),   ringHistory);
  sensors[little_fsr] = digitalSmooth((int16_t)analogRead(LITTLE_PIN), littleHistory);
  */

  // Use the simple AHRS function to get the current orientation.
  if (ahrs.getOrientation(&orientation)) {
    ahrs_pitch.update(orientation.pitch);
    ahrs_roll.update(orientation.roll);
    ahrs_yaw.update(orientation.heading);
    sensors[pitch] = (int16_t)ahrs_pitch.getValue();
    sensors[roll]  = (int16_t)ahrs_roll.getValue();
    sensors[yaw]   = (int16_t)ahrs_yaw.getValue();
    /*
    sensors[pitch] = digitalSmooth(((int16_t)orientation.pitch), pitchHistory);
    sensors[roll]  = digitalSmooth(((int16_t)orientation.roll), rollHistory);
    sensors[yaw]   = digitalSmooth(((int16_t)orientation.heading), yawHistory);
    */
  }

  /*Serial.println("pitch: "+String(sensors[pitch])+
                 ", roll: "+String(sensors[roll])+
                 ", yaw: "+String(sensors[yaw]));*/
  
  blePrintBytearray(sensors);
}

/***********************************************************
************************************************************
************************************************************
*********
*********
*********     End main loop / Begin global functions
*********
*********
************************************************************
************************************************************
***********************************************************/

void blePrintBytearray(int16_t *sensor_values){
 if (SERIAL_LOGGING) {
   String sensors_string = "";
   for (int i = 0; i < SENSOR_COUNT; ++i) {
     sensors_string += String(sensor_values[i]+", ");
   }
   //Serial.println(sensors_string);
 }
 
 uint8_t transmit_buf[(SENSOR_COUNT * 2)] = {0};
 for (int i = 0; i < SENSOR_COUNT; ++i) {
  transmit_buf[i * 2]       = sensor_values[i] & 255;          // 0x00FF
  transmit_buf[(i * 2) + 1] = (sensor_values[i] & 65280) >> 8; // 0xFF00
 }
 gatt.setChar(gattNotifiableCharId, transmit_buf, sizeof(transmit_buf));
}

int digitalSmooth(int16_t rawIn, int16_t *sensSmoothArray){
  /*
  * http://playground.arduino.cc/Main/DigitalSmooth
  * Paul Badger 2007
  * A digital smoothing filter for smoothing sensor jitter 
  * This filter accepts one new piece of data each time through a loop, which the 
  * filter inputs into a rolling array, replacing the oldest data with the latest reading.
  * The array is then transferred to another array, and that array is sorted from low to high. 
  * Then the highest and lowest %15 of samples are thrown out. The remaining data is averaged
  * and the result is returned.
  *
  * Every sensor used with the digitalSmooth function needs to have its own array to hold 
  * the raw sensor values. This array is then passed to the function, for it's use.
  * This is done with the name of the array associated with the particular sensor.
  */
  int j, k, temp, top, bottom;
  long total;
  static int i;
  static int sorted[filterSamples];
  boolean done;
  i = (i + 1) % filterSamples;    // increment counter and roll over if necc. -  % (modulo operator) rolls over variable
  sensSmoothArray[i] = rawIn;                 // input new data into the oldest slot
  for (j=0; j<filterSamples; j++){     // transfer data array into anther array for sorting and averaging
    sorted[j] = sensSmoothArray[j];
  }
  done = 0;                // flag to know when we're done sorting              
  while(done != 1){        // simple swap sort, sorts numbers from lowest to highest
    done = 1;
    for (j = 0; j < (filterSamples - 1); j++){
      if (sorted[j] > sorted[j + 1]){     // numbers are out of order - swap
        temp = sorted[j + 1];
        sorted [j+1] =  sorted[j] ;
        sorted [j] = temp;
        done = 0;
      }
    }
  }
  // throw out top and bottom 15% of samples - limit to throw out at least one from top and bottom
  bottom = max(((filterSamples * 15)  / 100), 1); 
  top = min((((filterSamples * 85) / 100) + 1  ), (filterSamples - 1));   // the + 1 is to make up for asymmetry caused by integer rounding
  k = 0;
  total = 0;
  for ( j = bottom; j< top; j++){
    total += sorted[j];  // total remaining indices
    k++; 
  }
  return total / k;    // divide by number of samples
}

/*
String packSensorData(short *sensor_values) {
 char transmit_buf[(SENSOR_COUNT * 2) + 1];
 for (int i = 0; i < SENSOR_COUNT; ++i) {
  transmit_buf[i * 2]       = sensor_values[i] & 255;          // 0x00FF
  transmit_buf[(i * 2) + 1] = (sensor_values[i] & 65280) >> 8; // 0xFF00
 }
 transmit_buf[(SENSOR_COUNT * 2)] = '/n';
 Serial.println(transmit_buf);
 return String(transmit_buf);
}

void unpackSensorData(String compressed){
  short output[SENSOR_COUNT] = {0};
  for (int i = 0; i < SENSOR_COUNT; ++i) {
      output[i] = (compressed[(i*2)+1]); // 0xFF00
      output[i] <<= 8;
      output[i] |= (compressed[i*2] & 255); // 0x00FF
  }
}
*/
