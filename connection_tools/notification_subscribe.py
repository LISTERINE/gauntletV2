# Example of low level interaction with a BLE UART device that has an RX and TX
# characteristic for receiving and sending data.  This doesn't use any service
# implementation and instead just manipulates the services and characteristics
# on a device.  See the uart_service.py example for a simpler UART service
# example that uses a high level service implementation.
# Author: Tony DiCola
import logging
import time
import uuid
from Queue import Queue, Empty
from struct import unpack
from ctypes import c_int8

import Adafruit_BluefruitLE


# Enable debug output.
#logging.basicConfig(level=logging.DEBUG)

# Define service and characteristic UUIDs used by the UART service.
UART_SERVICE_UUID = uuid.UUID('0000fff0-0000-1000-8000-00805f9b34fb')
RX_CHAR_UUID      = uuid.UUID('0000fff1-0000-1000-8000-00805f9b34fb')
#UART_SERVICE_UUID = "FFF0"
#RX_CHAR_UUID      = "FFF1"


class Notifier(object):
    SENSOR_FIELDS = 8
    BUF_SIZE = SENSOR_FIELDS*2
    STRUCT_FMT = "8h"
    MESSAGE_DELIM = "!"
    VALUE_DELIM = ","

    def __init__(self, queue):
        self.partial_data = []
        self.queue = queue

    def parse_result(self, data):
        bytes = unpack(Notifier.STRUCT_FMT, data)
        print bytes
        return bytes

    def callback(self, data):
        """ Function to receive RX characteristic changes.
        Note that this will be called on a different thread so be careful to
        make sure state that the function changes is thread safe.
        Use Queue/other thread-safe primitives to send data to other threads.
        """
        result = self.parse_result(data)
        if result:
            self.queue.put(result)

class Gauntlet(object):
    # Get the BLE provider for the current platform.
    ble = Adafruit_BluefruitLE.get_provider()

    def __init__(self):
        # Initialize the BLE system.  MUST be called before other BLE calls!
        Gauntlet.ble.initialize()
        self.queue = Queue()
        self.notifier= Notifier(self.queue)
        self.adapter = None
        self.device = None
        self.is_streaming = True

    def acquire_adapter(self):
        # Clear any cached data because both bluez and CoreBluetooth have issues with
        # caching data and it going stale.
        Gauntlet.ble.clear_cached_data()

        # Get the first available BLE network adapter and make sure it's powered on.
        self.adapter = Gauntlet.ble.get_default_adapter()
        self.adapter.power_on()
        print('Using adapter: {0}'.format(self.adapter.name))

        # Disconnect any currently connected UART devices.  Good for cleaning up and
        # starting from a fresh state.
        print('Disconnecting gauntlet if previously connected...')
        Gauntlet.ble.disconnect_devices([UART_SERVICE_UUID])

    def associate(self):
        print("scanning for gauntlet")
        try:
            self.adapter.start_scan()
            # Search for the first UART device found (will time out after 60 seconds
            # but you can specify an optional timeout_sec parameter to change it).
            self.device = Gauntlet.ble.find_device(service_uuids=[UART_SERVICE_UUID])
            if self.device is None:
                raise RuntimeError('Failed to find Gauntlet')
        finally:
            # Make sure scanning is stopped before exiting.
            self.adapter.stop_scan()

        print('Connecting to device...')
        self.device.connect()  # Will time out after 60 seconds, specify timeout_sec parameter
                          # to change the timeout.

    def subscribe(self):
        # Wait for service discovery to complete for at least the specified
        # service and characteristic UUID lists.  Will time out after 60 seconds
        # (specify timeout_sec parameter to override).
        print('Discovering services...')
        self.device.discover([UART_SERVICE_UUID], [RX_CHAR_UUID])

        # Find the UART service and its characteristics.
        print("setting up streamer")
        gauntlet_streamer = self.device.find_service(UART_SERVICE_UUID)
        print("chars:")
        print([char.uuid for char in gauntlet_streamer.list_characteristics()])
        print("getting rx")
        rx = gauntlet_streamer.find_characteristic(RX_CHAR_UUID)

        # Turn on notification of RX characteristics using the callback above.
        print('Subscribing to RX characteristic changes...')
        rx.start_notify(self.notifier.callback)

        # Now just wait for 30 seconds to receive data.
        print('Waiting 60 seconds to receive data from the device...')
        time.sleep(60)

    def reset(self):
        if self.adapter is not None:
            self.adapter.stop_scan()
        if self.device is not None:
            self.device.disconnect()

    def connect(self):
        #self.reset()
        self.acquire_adapter()
        self.associate()
        self.subscribe()

    def disconnect(self):
        self.device.disconnect()

    def stream(self):
        while 1:
            try:
                yield self.queue.get_nowait()
            except Empty:
                pass

    def time_stream(self, seconds=60):
        gauntlet_stream = self.stream()
        start = time.time()
        while start+seconds <= time.time():
            yield gauntlet_stream.next()

    def sample_stream(self, sample_count=100):
        gauntlet_stream = self.stream()
        counter = 0
        while counter <= sample_count:
            yield gauntlet_stream.next()

    def stop_stream(self):
        self.is_streaming = False

# Main function implements the program logic so it can run in a background
# thread.  Most platforms require the main thread to handle GUI events and other
# asyncronous events like BLE actions.  All of the threading logic is taken care
# of automatically though and you just need to provide a main function that uses
# the BLE provider.
def main():
    gauntlet = Gauntlet()
    gauntlet.connect()
    stream = gauntlet.time_stream()
    for sample in stream:
        print sample
    gauntlet.disconnect()

# Start the mainloop to process BLE events, and run the provided function in
# a background thread.  When the provided main function stops running, returns
# an integer status code, or throws an error the program will exit.
Gauntlet.ble.run_mainloop_with(main)
