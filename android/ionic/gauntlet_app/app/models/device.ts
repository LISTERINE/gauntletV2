//import {Serializable} from './json';
import {BLE} from 'ionic-native';

export class Descriptor {
  "uuid": string;
}

export class Characteristic {
  "service": string;
  "characteristic": string;
  "properties": Array<string>;
  "descriptors": Array<Descriptor>;
}

export class Device {
  "name": string;
  "id": string;
  "rssi": number;
  "advertising": Array<number>;
  "services": Array<string>;
  "characteristics": Array<Characteristic>;
  "receiveBuffer": Array<string> = [];
  "messages": Array<string> = [];

/*
  writeToConnected(data) {
    BLE.writeWithoutResponse(this.id,
                             this.uartService,
                             this.txUUIDcharacteristic,
                             data);
  };
*/
  fromJSON(deviceJSON) {
    let device = new Device();
    let dj = JSON.parse(deviceJSON);
    device.name = dj.name;
    device.id = dj.id;
    device.rssi = dj.rssi;
    device.advertising = dj.advertising;
    device.services = dj.services;
    device.characteristics = dj.characteristics;
    device.receiveBuffer = [];
    device.messages = [];
    return device;
  };

}

export class DeviceMeta {
  "uartService": string = "6E400001-B5A3-F393-E0A9-E50E24DCCA9E";
  "rxUUIDcharacteristic": string = "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"// Notify
  "txUUIDcharacteristic": string = "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"// write without response

  stringToBytes(string) {
   var array = new Uint8Array(string.length);
   for (var i = 0, l = string.length; i < l; i++) {
       array[i] = string.charCodeAt(i);
    }
    return array.buffer;
  }

  bytesToString(buffer) {
    return String.fromCharCode.apply(null, new Uint8Array(buffer));
  };
};
