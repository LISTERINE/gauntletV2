import {Component, NgZone} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {BLE} from 'ionic-native';
import {Device, DeviceMeta} from '../../models/device';

@Component({
  templateUrl: 'build/pages/console/console.html'
})

export class ConsolePage {
  constructor(private navCtrl: NavController, private navParams: NavParams, private _zone: NgZone) {
  }

  connectedDevice: Device = new Device().fromJSON(this.navParams.get('device'));
  deviceMeta: DeviceMeta = new DeviceMeta();
  serialConnected: boolean = false;
  EOL: string = "!";
  viewLength: number = 10;

  serialWatch() {
    this.serialConnected = true;
    console.log("attempting serial connection");
    console.log(JSON.stringify(this.connectedDevice));
    BLE.startNotification(this.connectedDevice.id,
                          this.deviceMeta.uartService,
                          this.deviceMeta.rxUUIDcharacteristic)
      .subscribe((rawData) => {
        var stringData = this.deviceMeta.bytesToString(rawData);
        if (stringData.includes(this.EOL)){
          // end of last message, beginning of next message
          var [endMessage, newMessage] = stringData.split(this.EOL);
          this.connectedDevice.receiveBuffer.push(endMessage);
          let completeData = this.connectedDevice.receiveBuffer.join('');
          try {
            // If we can't parse it, it's bad json.
            JSON.parse(completeData);
            this._zone.run(() => {
              this.connectedDevice.messages.push(completeData);
              if (this.connectedDevice.messages.length >= this.viewLength) {
                this.connectedDevice.messages.shift();
              };
            });
            console.log("complete data:", completeData);
          } catch(e) {
            console.log(JSON.stringify(e));
          }
          // beginning of next json string
          this.connectedDevice.receiveBuffer = [];
          this.connectedDevice.receiveBuffer.push(newMessage);
        } else {
          this.connectedDevice.receiveBuffer.push(stringData);
        }
      }, (e) =>{
        console.log("error:", e);
        this.serialConnected = false;
      });
  };

  record() {};
}
