import {Component, NgZone} from '@angular/core';
import {NavController} from 'ionic-angular';
import {BLE} from 'ionic-native';
import {Device} from '../../models/device';
import {ConsolePage} from '../console/console';

@Component({
  templateUrl: 'build/pages/home/home.html'
})
export class HomePage {

  scanDevices: Device[]=[];
  connectedDevice: Device = new Device();
  scanning: boolean = false;
  connecting: boolean = false;
  disconnecting: boolean = false;
  messages: Array<string> = [];
  uartService: string = "6E400001-B5A3-F393-E0A9-E50E24DCCA9E";
  rxUUIDcharacteristic: string = "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"// Notify
  txUUIDcharacteristic: string = "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"// write without response

  constructor(private navCtrl: NavController, private _zone: NgZone) {
  }

  discoveredDevice(device){
    this._zone.run(() => {
      this.scanDevices.push();
    });
  };

  // Scan for devices
  bleScan(e) {
    console.log("ble scanning");
    this.scanDevices = [];
    this.scanning = true;
    BLE.scan([], 5).subscribe(device => {
        this._zone.run(() => {
          console.log("found ", JSON.stringify(device));
          this.scanDevices.push(device);
          this.scanning = false;
        });
      }, (error) => {
        console.log("error connecting to device", error);
    });
  };

  // Connect to a device
  connectToDevice(device) {
    this.connecting = true;
    console.log("attempting to connect to", JSON.stringify(device));
    BLE.connect(device.id).subscribe((connectedDevice) => {
        this._zone.run(() => {
          console.log("connected", JSON.stringify(connectedDevice));
          this.connectedDevice = connectedDevice;
          this.connecting = false;
        });
      }, (connectFailure) => {
        this._zone.run(() => {
          console.log("connect failed", JSON.stringify(connectFailure));
          this.connecting = false;
        });
    });
  };

  // Disconnect to devices
  disconnectDevice(device) {
    this.disconnecting = true;
    console.log("attempting to disconect from", JSON.stringify(device));
    BLE.disconnect(device.id).then(() => {
      this.connectedDevice = new Device();
      this.disconnecting = false;
    });
  };

  viewInConsole(device) {
    this.navCtrl.push(ConsolePage, {"device": JSON.stringify(device)});
  }

};
