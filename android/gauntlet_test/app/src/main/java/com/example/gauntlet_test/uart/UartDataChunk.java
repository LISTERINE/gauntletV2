package com.example.gauntlet_test.uart;

import android.util.Log;

class UartDataChunk {
    static final int TRANSFERMODE_TX = 0;
    static final int TRANSFERMODE_RX = 1;
    // incoming data translation constants
    private final static Integer SENSOR_COUNT = 7;
    private final static Integer SENSOR_CHAR_BUF_SIZE = 2;
    private final static Integer BUF_LEN = SENSOR_CHAR_BUF_SIZE*SENSOR_COUNT;

    private long mTimestamp;        // in millis
    private int mMode;
    private byte[] mData;

    // Log
    private final static String TAG = UartDataChunk.class.getSimpleName();

    UartDataChunk(long timestamp, int mode, byte[] bytes) {
        mTimestamp = timestamp;
        mMode = mode;
        mData = bytes;
    }

    long getTimestamp() {
        return mTimestamp;
    }

    public int getMode() {
        return mMode;
    }

    public byte[] getData() {
        return mData;
    }

}
