/*
 * Copyright (C) 2013 youten
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ble;

import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.pm.PackageManager;

import java.util.ArrayList;

/**
 * Util for Bluetooth Low Energy
 */
public class BleUtil {

    public static final Integer SENSOR_COUNT = 8;

    private BleUtil() {
        // Util
    }

    public static String decodeMessageToString(byte[] str_buf){
        int signals_out[] = new int[SENSOR_COUNT];
        StringBuilder printable = new StringBuilder(50);
        for (int i = 0; i < SENSOR_COUNT; ++i) {
            signals_out[i] = (int) (str_buf[(i*2)+1]); // 0xFF00
            signals_out[i] <<= 8;
            signals_out[i] |= (str_buf[i*2] & 255); // 0x00FF
            printable.append(signals_out[i]);
            if (i < SENSOR_COUNT-1){
                printable.append(",");
            }
        }
        printable.append('\n');
        return printable.toString();
    }

    public static int[] decodeMessageToInts(byte[] str_buf){
        int signals_out[] = new int[SENSOR_COUNT];
        for (int i = 0; i < SENSOR_COUNT; ++i) {
            signals_out[i] = (int) (str_buf[(i*2)+1]); // 0xFF00
            signals_out[i] <<= 8;
            signals_out[i] |= (str_buf[i*2] & 255); // 0x00FF
        }
        return signals_out;
    }

    /** check if BLE Supported device */
    public static boolean isBLESupported(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
    }

    /** get BluetoothManager */
    public static BluetoothManager getManager(Context context) {
        return (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
    }
}
