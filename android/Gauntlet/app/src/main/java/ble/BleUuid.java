/*
 * Copyright (C) 2013 youten
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ble;

/** BLE UUID Strings */
public class BleUuid {

	// UUIDs for UART service and associated characteristics.
	public static final String SERVICE_UART = "6E400001-B5A3-F393-E0A9-E50E24DCCA9E";
	public static final String CHAR_TX = "6E400002-B5A3-F393-E0A9-E50E24DCCA9E";
	public static final String CHAR_RX = "6E400003-B5A3-F393-E0A9-E50E24DCCA9E";

	public static final String GAUNTLET_SERVICE = "0000FFF0-0000-1000-8000-00805F9B34FB";
	//public static final String GAUNTLET_SERVICE =   "0000FF0F-0000-0100-0800-0008F5B943BF";
	//public static final String GAUNTLET_RX =        "0000FF1F-0000-0100-0800-0008F5B943BF";
	public static final String GAUNTLET_RX = "0000FFF1-0000-1000-8000-00805F9B34FB";


	public static final String CLIENT_CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";

}
