/*
 * Copyright (C) 2013 youten
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ble;

import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jonathanferretti.gauntlet.Actions;
import com.jonathanferretti.gauntlet.DistanceData;
import com.jonathanferretti.gauntlet.GestureListAdapter;
import com.jonathanferretti.gauntlet.Ingester;
import com.jonathanferretti.gauntlet.R;

import java.util.UUID;

import static ble.BleUtil.decodeMessageToInts;
import static ble.BleUtil.decodeMessageToString;
import static ble.BleUuid.CLIENT_CHARACTERISTIC_CONFIG;

public class DeviceActivity extends Activity implements View.OnClickListener {
	private static final String TAG = "BLEDevice";

	public static final String EXTRA_BLUETOOTH_DEVICE = "BT_DEVICE";
	private BluetoothAdapter mBTAdapter;
	private BluetoothDevice mDevice;
	private BluetoothGatt mConnGatt;
	private int mStatus;

    private Ingester ingester = new Ingester();
    private Actions notifyActionManager;

	private Button mToggleRecording;
    private Button mToggleTraining;
    private Button mRefreshGestureList;
	private ListView mGestureList;
    GestureListAdapter mAdapter;
    private boolean preview_on = true;
    public TextView data_preview;
    public TextView samples_per_second_preview;
    public Integer samples_per_second = 0;
    public long previous_time = 0;
    public long new_time = 0;
    public static String mNewFileName = null;
    private static final int mGestureDistanceLimit = 20000;

	private BluetoothGattCharacteristic mReadRXCharacteristic;
	private BluetoothGattCharacteristic mNotifyCharacteristic;

	public BluetoothGattCharacteristic getmReadRXCharacteristic() {
		return mReadRXCharacteristic;
	}

	public void setmReadRXCharacteristic(BluetoothGattCharacteristic mReadRXCharacteristic) {
        Log.d("ble", "set read char");
		this.mReadRXCharacteristic = mReadRXCharacteristic;
	}


	private final BluetoothGattCallback mGattcallback = new BluetoothGattCallback() {
		@Override
		public void onConnectionStateChange(BluetoothGatt gatt, int status,
				int newState) {
			if (newState == BluetoothProfile.STATE_CONNECTED) {
                Log.d("ble", "conencted");
				mStatus = newState;
				mConnGatt.discoverServices();
                mConnGatt.requestConnectionPriority(BluetoothGatt.CONNECTION_PRIORITY_HIGH); // Crank up the transmission speed (uses more power)
			} else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
				mStatus = newState;
				runOnUiThread(new Runnable() {
					public void run() {
                        mToggleTraining.setEnabled(false);
                        mToggleRecording.setEnabled(false);
					};
				});
			}
		}

		@Override
		public void onServicesDiscovered(BluetoothGatt gatt, int status) {
			Log.d("SERIVICE UUID", "Discovering");
			for (BluetoothGattService service : gatt.getServices()) {
				if ((service == null) || (service.getUuid() == null)) {
					continue;
				}
				Log.d("SERVICE UUID", service.getUuid().toString());
				if (BleUuid.GAUNTLET_SERVICE.equalsIgnoreCase(service.getUuid().toString())){
                    Log.d("ble", "has uart");
					setmReadRXCharacteristic(service.getCharacteristic(UUID.fromString(BleUuid.GAUNTLET_RX)));
					runOnUiThread(new Runnable() {
						public void run() {
                            mToggleTraining.setEnabled(true);
							mToggleRecording.setEnabled(true);
						}
					});
				}
			}
		}

		@Override
		public void onCharacteristicRead(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
            Log.d("ble", "attempting read");
			if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d("ble", "reading success");
				if (BleUuid.GAUNTLET_RX.equalsIgnoreCase(characteristic.getUuid().toString())) {
                    Log.d("ble", "we read rx");
                    Log.d("GATT read bytearray", decodeMessageToString(characteristic.getValue()));
					Log.d("GATT read", characteristic.getValue().toString());
				}
			}
		}

		@Override
		public void onCharacteristicChanged(BluetoothGatt gatt,
											BluetoothGattCharacteristic characteristic){
			if (BleUuid.GAUNTLET_RX.equalsIgnoreCase(characteristic.getUuid().toString())) {
                byte[] bleMessage = characteristic.getValue();
                ingester.addData(decodeMessageToInts(bleMessage));
                if (preview_on) {
                    final String recvd = decodeMessageToString(bleMessage);
                    //Log.d("GATT_NOTIFY", recvd);
                    samples_per_second += 1;
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            new_time = System.currentTimeMillis() / 1000;
                            if (new_time >= previous_time + 1) {
                                samples_per_second_preview.setText("Samples per second: " + samples_per_second.toString());
                                samples_per_second = 0;
                                previous_time = new_time;
                            }
                            data_preview.setText(recvd);
                        }
                    });
                }
			}
		}

		@Override
		public void onCharacteristicWrite(BluetoothGatt gatt,
				BluetoothGattCharacteristic characteristic, int status) {
            Log.d("ble", "we wrote something");
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_device);

		// state
		mStatus = BluetoothProfile.STATE_DISCONNECTED;
        mToggleTraining = (Button) findViewById(R.id.toggle_training_button);
        mToggleTraining.setOnClickListener(this);
        mToggleRecording = (Button) findViewById(R.id.toggle_recording_button);
        mToggleRecording.setOnClickListener(this);
        mRefreshGestureList = (Button) findViewById(R.id.refresh_gesture_list_button);
        mRefreshGestureList.setOnClickListener(this);
        data_preview = (TextView) findViewById(R.id.data_preview);
        samples_per_second_preview = (TextView) findViewById(R.id.samples_per_second);

		ingester.loadGestures();
		mGestureList = (ListView) findViewById(R.id.gesture_list);
		mAdapter = new GestureListAdapter(ingester.gestureFileNames);
		mGestureList.setAdapter(mAdapter);
        notifyActionManager = new Actions((NotificationManager) getSystemService(NOTIFICATION_SERVICE));
	}

	@Override
	protected void onResume() {
		super.onResume();

		init();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (mConnGatt != null) {
			if ((mStatus != BluetoothProfile.STATE_DISCONNECTING)
					&& (mStatus != BluetoothProfile.STATE_DISCONNECTED)) {
				mConnGatt.disconnect();
			}
			mConnGatt.close();
			mConnGatt = null;
		}
	}

    public void openDataChannel(){
        if (getmReadRXCharacteristic() != null) {
            Log.d("ble", "we have the read char");
            BluetoothGattCharacteristic rxCharacteristic = getmReadRXCharacteristic();
            final int charaProp = rxCharacteristic.getProperties();
            Log.d("THE PROP", ((Integer)charaProp).toString());
            if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                Log.d("ble", "there was another reader enabled. killing it.");
                // If there is an active read notification on a characteristic, clear
                // it first so we only read one at a time
                if (mNotifyCharacteristic != null) {
                    // clear the read
                    mConnGatt.setCharacteristicNotification(mNotifyCharacteristic, false);
                    Log.d("ble", "killed");
                    mNotifyCharacteristic = null;
                }
                Log.d("ble", "enabled reading of rx char");
                mConnGatt.readCharacteristic(rxCharacteristic);
            }
            if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                // set notify
                mNotifyCharacteristic = rxCharacteristic;
                mConnGatt.setCharacteristicNotification(rxCharacteristic, true);
                BluetoothGattDescriptor descriptor = rxCharacteristic.getDescriptor(UUID.fromString(CLIENT_CHARACTERISTIC_CONFIG));
                descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                mConnGatt.writeDescriptor(descriptor);
                Log.d("ble", "enabled notification");
            }
        }
    }

    public void closeDataChannel(){
        if (getmReadRXCharacteristic() != null) {
            BluetoothGattCharacteristic rxCharacteristic = getmReadRXCharacteristic();
            final int charaProp = rxCharacteristic.getProperties();
            if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                // clear notify
                mNotifyCharacteristic = rxCharacteristic;
                mConnGatt.setCharacteristicNotification(rxCharacteristic, false);
            }
        }
    }
    public void showNewFileDialog(Context context){
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.new_file_menu);
        dialog.setTitle("Set new gesture name");
        String gesture_file_name = new String();
        final EditText file_name_input = (EditText) dialog.findViewById(R.id.gesture_name_input);
        Button acceptButton = (Button) dialog.findViewById(R.id.accept_gesture_name_button);
        Button cancelButton = (Button) dialog.findViewById(R.id.cancel_gesture_name_button);

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newFileName = file_name_input.getText().toString();
                if (newFileName.length() < 1) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Name must be at least one character", Toast.LENGTH_SHORT).show();
                        }
                    });
                    return;
                }
                ingester.startRecording(newFileName);
                openDataChannel();
                dialog.dismiss();
            }
        });
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.toggle_recording_button) {
			if (ingester.isRecording){
				// stop recording
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mToggleRecording.setText(R.string.start_recording);
                    }
                });
				final DistanceData distanceData = ingester.stopRecording(getApplicationContext());
                if (distanceData == null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Bad recording, try again.", Toast.LENGTH_SHORT).show();
                        }
                    });
                    return;
                }
				Log.d("DISTANCE_RESULT", "distance was: "+distanceData.toString());
				final String gestureName = extensionTrim(distanceData.getName(), ingester.dataFileExtension);
				if (distanceData.getDistance() < mGestureDistanceLimit) {
					Log.d("DISTANCE_RESULT", "acceptable distance: "+distanceData.getDistance().toString());
                    notifyActionManager.issueNotification(getApplicationContext(), gestureName);
				} else {
					Log.d("DISTANCE_RESULT", "distance: "+distanceData.getDistance().toString()+", not accepted");
				}
                runOnUiThread(new Runnable() {
                    public void run() {
                        String report = "Distance was: "+distanceData.getDistance().toString()+", matched: "+gestureName;
                        Toast.makeText(getApplicationContext(), report, Toast.LENGTH_LONG).show();
                    }
                });
				closeDataChannel();
			} else {
				// start recording
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mToggleRecording.setText(R.string.stop_recording);
                    }
                });
                Log.d("ble", "hit record button");
                ingester.startRecording();
                openDataChannel();
			}
        } else if (v.getId() == R.id.toggle_training_button) {
            Log.d("ble", "hit train button");
			if (ingester.isRecording) {
				// stop training
                final DistanceData distanceData = ingester.stopRecording(getApplicationContext());
                if (distanceData == null) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), "Bad recording, try again.", Toast.LENGTH_SHORT).show();
                        }
                    });
                    ingester.loadGestures();
                    mAdapter.setFileList(ingester.listSavedGestureFiles());
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mToggleTraining.setText(R.string.start_training);
                        }
                    });
                    return;
                }
                ingester.loadGestures();
                mAdapter.setFileList(ingester.listSavedGestureFiles());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mToggleTraining.setText(R.string.start_training);
                    }
                });
				Log.d("DISTANCE_RESULT", "distance was: " + distanceData.getDistance().toString());
				closeDataChannel();
			} else {
                // start training
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mToggleTraining.setText(R.string.stop_training);
                    }
                });
                ingester.startTraining();
                showNewFileDialog(this);
			}
		} else if (v.getId() == R.id.refresh_gesture_list_button) {
            Log.d(TAG, "hit refresh button");
            Toast.makeText(getApplicationContext(), "Refreshing...", Toast.LENGTH_SHORT).show();
            ingester.loadGestures();
            mAdapter.setFileList(ingester.listSavedGestureFiles());
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                }
            });
        }
	}



	private void init() {
		// BLE check
		if (!BleUtil.isBLESupported(this)) {
			Toast.makeText(this, R.string.ble_not_supported, Toast.LENGTH_SHORT)
					.show();
			finish();
			return;
		}

		// BT check
		BluetoothManager manager = BleUtil.getManager(this);
		if (manager != null) {
			mBTAdapter = manager.getAdapter();
		}
		if (mBTAdapter == null) {
			Toast.makeText(this, R.string.bt_unavailable, Toast.LENGTH_SHORT)
					.show();
			finish();
			return;
		}

		// check BluetoothDevice
		if (mDevice == null) {
			mDevice = getBTDeviceExtra();
			if (mDevice == null) {
				finish();
				return;
			}
		}

		// button disable
		mToggleTraining.setEnabled(false);
		mToggleRecording.setEnabled(false);

		// connect to Gatt
		if ((mConnGatt == null)
				&& (mStatus == BluetoothProfile.STATE_DISCONNECTED)) {
			// try to connect
			mConnGatt = mDevice.connectGatt(this, false, mGattcallback);
			mStatus = BluetoothProfile.STATE_CONNECTING;
		} else {
			if (mConnGatt != null) {
				// re-connect and re-discover Services
				mConnGatt.connect();
				mConnGatt.discoverServices();
			} else {
				Log.e(TAG, "state error");
				finish();
				return;
			}
		}
	}

	private BluetoothDevice getBTDeviceExtra() {
		Intent intent = getIntent();
		if (intent == null) {
			return null;
		}

		Bundle extras = intent.getExtras();
		if (extras == null) {
			return null;
		}

		return extras.getParcelable(EXTRA_BLUETOOTH_DEVICE);
	}

	public String extensionTrim(String filename, String extensionWithDot){
        if (filename.endsWith(extensionWithDot))
            return filename.substring(0,filename.length() - extensionWithDot.length());
        else
            return filename;
    }

}
