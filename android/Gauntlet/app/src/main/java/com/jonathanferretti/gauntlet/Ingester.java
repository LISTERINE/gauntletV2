package com.jonathanferretti.gauntlet;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import net.sf.javaml.distance.fastdtw.dtw.TimeWarpInfo;
import net.sf.javaml.distance.fastdtw.timeseries.TimeSeries;
import net.sf.javaml.distance.fastdtw.timeseries.TimeSeriesPoint;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.CSVWriter;
import ble.BleUtil;

import static net.sf.javaml.tools.data.FileHandler.exportDataset;
import static net.sf.javaml.tools.data.FileHandler.loadDataset;

/**
 * Created by jon on 2/7/17.
 */

public class Ingester {
    private static final String TAG = "Ingester";
    public final static String dataFileExtension = ".csv";
    private static final String fileHeadDir = "gauntlet";
    public static final String fileRootDir = Environment.getExternalStoragePublicDirectory(fileHeadDir).getAbsolutePath();
    private TimeSeries incomingTimeSeries;
    private static final ArrayList<String> labels = new ArrayList<>(Arrays.asList("Time", "Pitch", "Roll", "Yaw", "Thumb", "Index", "Middle", "Ring", "Little"));
    public double latestDistance = 0;
    public static ArrayList<String> gestureFileNames = new ArrayList<>();
    private static ArrayList<TimeSeries> gestures;
    public boolean isTraining = false;
    public boolean isRecording = false;
    private static final Integer timeWarpDimensions = labels.size();
    public static String fileNameIncomingTimeSeries = null;
    private static final int warpSearchRadius = 30;

    public Ingester(){
        gestures = new ArrayList<>();
        // Create the data dir if it doesn't exist
        Log.d(TAG, "Root: "+fileRootDir);
        File rootDir = new File(fileRootDir);
        if (!rootDir.exists()) {
            Log.d(TAG, fileRootDir+" Doesn't exist. Creating...");
            if (!rootDir.mkdir()) {
                Log.e(TAG, "Directory not created");
            }
        } else {
            Log.d(TAG, fileRootDir+" Exists");
        }

    }

    private static File createNewTimeSeriesFile(String fName){
        File dataFile = new File(fileRootDir, fName);
        try {
            dataFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dataFile;
    }

    public void loadGestures(){
        this.gestureFileNames.clear();
        this.gestures.clear();
        String[] gestureFiles = listSavedGestureFiles();
        for (int i = 0; i < gestureFiles.length; i++){
            Log.d(TAG, "Loading file: "+gestureFiles[i]);
            this.gestureFileNames.add(gestureFiles[i]);
            TimeSeries gesture = Ingester.loadTimeSeries(new File(fileRootDir, gestureFiles[i]));
            gesture.setLabels(labels);
            gestures.add(gesture);
        }
    }

    public String[] listSavedGestureFiles(){
        FilenameFilter gestureFileFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                String lowercaseName = name.toLowerCase();
                if (lowercaseName.endsWith(dataFileExtension)) {
                    return true;
                } else {
                    return false;
                }
            }
        };
        String[] gestureFiles = new File(fileRootDir).list(gestureFileFilter);
        if (gestureFiles == null){
            return new String[0];
        }
        Arrays.sort(gestureFiles);
        return gestureFiles;
    }

    public static String convertStreamToString(InputStream is) throws IOException {
        // http://www.java2s.com/Code/Java/File-Input-Output/ConvertInputStreamtoString.htm
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        Boolean firstLine = true;
        while ((line = reader.readLine()) != null) {
            if(firstLine){
                sb.append(line);
                firstLine = false;
            } else {
                sb.append("\n").append(line);
            }
        }
        reader.close();
        return sb.toString();
    }

    public static String getStringFromFile (String filePath) throws IOException {
        File fl = new File(fileRootDir, filePath);
        FileInputStream fin = new FileInputStream(fl);
        String ret = convertStreamToString(fin);
        //Make sure you close all streams.
        fin.close();
        return ret;
    }

    public void startTraining(){
        //TODO create training file
        isTraining = true;
        isRecording = true;
    }

    public void startRecording(String fileName){
        fileNameIncomingTimeSeries = fileName;
        isRecording = true;
        incomingTimeSeries = new TimeSeries(timeWarpDimensions); // add 1 for time field
        incomingTimeSeries.setLabels(labels);
    }

    public void startRecording(){
        isRecording = true;
        incomingTimeSeries = new TimeSeries(timeWarpDimensions); // add 1 for time field
        incomingTimeSeries.setLabels(labels);
    }

    public void addData(int[] datum){
        double[] d_datum = new double[BleUtil.SENSOR_COUNT];
        for (int i = 0; i < datum.length; i++) {
            d_datum[i] = datum[i];
        }
        TimeSeriesPoint tsp = new TimeSeriesPoint(d_datum);
        incomingTimeSeries.addLast(incomingTimeSeries.size(), tsp);
    }

    public DistanceData compareGesture(){
        DistanceData distanceData = new DistanceData((double)0, null, null);
        for (int i = 0; i < gestures.size(); i++){
            Log.d(TAG, "Comparing to gesture set: "+((Integer)i).toString());
            double distance;
            try {
                final TimeWarpInfo info = net.sf.javaml.distance.fastdtw.dtw.FastDTW.getWarpInfoBetween(gestures.get(i), incomingTimeSeries, warpSearchRadius);
                distance = info.getDistance();
            }
            catch (ArrayIndexOutOfBoundsException e) {
                return null;
            } catch (IndexOutOfBoundsException e) {
                return null;
            }
            if (distanceData.getTimeSeries() == null){
                Log.d(TAG, "Setting new distance: "+ new Double(distance).toString());
                distanceData.setTimeSeries(gestures.get(i));
                distanceData.setDistance(distance);
                distanceData.setName(gestureFileNames.get(i));
                continue;
            }
            if (distance < distanceData.getDistance()){
                Log.d(TAG, "Setting new distance: "+ new Double(distance).toString());
                distanceData.setTimeSeries(gestures.get(i));
                distanceData.setDistance(distance);
                distanceData.setName(gestureFileNames.get(i));
            }
        }
        return distanceData;
    }

    public DistanceData stopRecording(Context context){
        isRecording = false;
        DistanceData distance = new DistanceData((double)-1, null);
        if (isTraining) {
            Log.d(TAG, "training. returning -1.");
            isTraining = false;
            if (fileNameIncomingTimeSeries == null)
            {
                Log.d(TAG, "null file name found, not saving");
                Toast.makeText(context, "null file name found, not saving", Toast.LENGTH_SHORT).show();
                return distance;
            }
            Ingester.saveTimeSeries(incomingTimeSeries, fileNameIncomingTimeSeries);
        } else {
            Log.d(TAG, "not training. comparing...");
            distance = compareGesture();
        }
        return distance;
    }

    public static boolean saveTimeSeries(TimeSeries timeseries, String fileName){
        File outputFile = createNewTimeSeriesFile(fileName+dataFileExtension);
        CSVWriter csvWriter;
        try {
            outputFile.createNewFile();
            csvWriter = new CSVWriter(new FileWriter(outputFile), '\t');
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        String[] header = new String[labels.size()];
        for (int i = 0; i < labels.size(); i++){
            header[i] = labels.get(i);
        }
        csvWriter.writeNext(header);

        for (int i = 0; i < timeseries.size(); i++){
            String[] vectorAsString = new String[timeWarpDimensions];
            vectorAsString[0] = ((Integer)i).toString();
            for (int j = 0; j < timeWarpDimensions-1; j++){
                double[] dvect = timeseries.getMeasurementVector(i);
                String vect = ((Double)timeseries.getMeasurementVector(i)[j]).toString();
                vectorAsString[j+1] = vect;
            }
            csvWriter.writeNext(vectorAsString);
        }
        try {
            csvWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public static TimeSeries loadTimeSeries(File tsFile){
        TimeSeries timeseries = new TimeSeries(BleUtil.SENSOR_COUNT);
        InputStream inputStream;
        BufferedReader csv;
        List<String[]> points;
        try {
            inputStream = new FileInputStream(tsFile);
            csv = new BufferedReader(new InputStreamReader(inputStream));
            CSVReader reader = new CSVReader(csv, '\t', '\"', 1); // start at 1 to skip header row
            points = reader.readAll();
        } catch (FileNotFoundException e) {
            Log.d(TAG, "Error loading file: "+tsFile.getName());
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        for (int i=0; i<points.size(); i++){
            double[] pointDoubles = new double[BleUtil.SENSOR_COUNT];
            double time = Double.parseDouble(points.get(i)[0]);
            for (int j = 0; j < BleUtil.SENSOR_COUNT; j++){
                pointDoubles[j] = Double.parseDouble(points.get(i)[j+1]);
            }
            TimeSeriesPoint point = new TimeSeriesPoint(pointDoubles);
            timeseries.addLast(time, point);
        }
        return timeseries;
    }

    public void deleteFile(String fileName){
        File f = new File(fileRootDir, fileName);
        f.delete();
    }
}