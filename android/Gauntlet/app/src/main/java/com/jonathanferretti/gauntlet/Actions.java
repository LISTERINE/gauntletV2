package com.jonathanferretti.gauntlet;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;

/**
 * Created by jon on 3/16/17.
 */

public class Actions {

    private NotificationManager mNotifyMgr;
    private static final long vibratePattern[] = {0};
    private static Integer mNotificationId = 1000;
    public static final String CUSTOM_INTENT = "com.gauntlet.gcomplete";

    public Actions(NotificationManager notificationManager){
        // Gets an instance of the NotificationManager service
        mNotifyMgr = notificationManager;
    }

    public void issueNotification(Context context, String body){
        Intent i = new Intent();
        i.setAction(CUSTOM_INTENT);
        i.putExtra("gesture_name", body);
        context.sendBroadcast(i);
    }

}
