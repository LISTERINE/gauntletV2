package com.jonathanferretti.gauntlet;

import net.sf.javaml.distance.fastdtw.timeseries.TimeSeries;

/**
 * Created by jon on 4/1/17.
 */

public class DistanceData {

    private Double distance;
    private String name;
    private TimeSeries timeSeries;

    public DistanceData() {
        this.distance = null;
        this.name = null;
    }

    public DistanceData(Double distance, String name){
        this.distance = distance;
        this.name = name;
    }

    public DistanceData(double distance, String name, TimeSeries timeSeries){
        this.distance = distance;
        this.name = name;
        this.timeSeries = timeSeries;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TimeSeries getTimeSeries() {
        return timeSeries;
    }

    public void setTimeSeries(TimeSeries timeSeries) {
        this.timeSeries = timeSeries;
    }
}
