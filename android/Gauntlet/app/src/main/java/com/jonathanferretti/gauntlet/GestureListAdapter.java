package com.jonathanferretti.gauntlet;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by jon on 3/30/17.
 */

public class GestureListAdapter extends BaseAdapter {
    private static final String TAG = GestureListAdapter.class.getSimpleName();
    public static ArrayList<String> gestureFiles;
    Context context;
    private static LayoutInflater inflater=null;

    public GestureListAdapter(ArrayList<String> gestureFiles){
        Collections.sort(gestureFiles);
        this.gestureFiles = gestureFiles;
    }

    public void setFileList(String[] filenames){
        Arrays.sort(filenames);
        this.gestureFiles = new ArrayList<>();
        for (int i=0; i < filenames.length; i++){
            this.gestureFiles.add(filenames[i]);
        }
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount(){
        if (gestureFiles != null) {
            return gestureFiles.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    public class Holder
    {
        TextView gestureName;
        Button deleteButton;
    }

    @Override
    public View getView(final int position, final View convertView, final ViewGroup parent){
        context = parent.getContext();
        inflater = (LayoutInflater)context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Holder holder = new Holder();
        final View rowView = inflater.inflate(R.layout.gesture_listview_adapter, null);
        holder.gestureName = (TextView) rowView.findViewById(R.id.gesture_name);
        holder.gestureName.setText(this.gestureFiles.get(position));
        holder.deleteButton = (Button) rowView.findViewById(R.id.delete_gesture_button);

        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "This would delete the file if deleting were implemented");
                TextView text = (TextView) rowView.findViewById(R.id.gesture_name);
                File f = new File(Ingester.fileRootDir, text.getText().toString());
                f.delete();
                Ingester ingester = new Ingester();
                ingester.loadGestures();
                setFileList(ingester.listSavedGestureFiles());
                //rowView.setBackgroundColor(ContextCompat.getColor(context, R.color.deletedFile));
            }
        });
        return rowView;
    }
}
