%module grt
%include "typemaps.i"
%include "std_vector.i"
%include "std_string.i"
%include "arrays_java.i"
%include "various.i"
%{
#include "../GRT/GRT.h"
#include "../GRT/DataStructures/VectorFloat.h"
#include "../GRT/DataStructures/Matrix.h"
#include <vector.h>
typedef unsigned int UINT;
using namespace GRT;
%}


// look here for how types should be mapped if you need more http://swig.org/Doc1.3/Java.html#java_typemaps
// setup type map for uint
%typemap(jni) UINT "jlong"
%typemap(jtype) UINT "long"
%typemap(jstype) UINT "long"
%typemap(out) UINT %{ $result = (jlong)$1; %}
%typemap(in) UINT "(jlong)$input"

%typemap(jni) size_type "jint"
%typemap(jtype) size_type "int"
%typemap(jstype) size_type "int"
%typemap(out) size_type %{ $result = (jint)$1; %}
%typemap(in) size_type "(jint)$input"


// ———— Vector ————————————
%include "../GRT/Util/GRTTypedefs.h"
%include "../GRT/Util/GRTCommon.h"
// instantiate Vector, just to give SWIG something to do with it

%template(VectorT) std::vector<float>;
//%include "../GRT/DataStructures/Vector.h"
//%include "../GRT/DataStructures/VectorFloat.h"
//%include "../GRT/Util/MinMax.h"
/*
%typemap(jni) Vector "jdoubleArray"
%typemap(jtype) Vector "double[]"
%typemap(jstype) Vector "double[]"
%typemap(out) Vector<T> &
%{
$result = jenv->NewDoubleArray($1->size());
jenv->SetDoubleArrayRegion($result, 0, $1->size(), &(*$1)[0]);
%}
%typemap(out) Vector %{ $result = (jlong)$1; %}
%typemap(in) Vector "(jlong)$input"
*/
%typemap(jstype) VectorT "Vector"
%typemap(jtype) VectorT "Vector"
%typemap(jni) VectorT "Vector"
// ———— Matrix ———————————–
%include "../GRT/DataStructures/Matrix.h"
// instantiate Matrix, just to give SWIG something to do with it
%template(MatrixBase) <VectorT>;
//%include "../GRT/DataStructures/MatrixFloat.h"

// ——————————————————-
/* For the rest, let’s just grab the original header files here and
* let swig generate the glue code for these. Here I selected only
* those headers which are necessary for DTW, and their base classes
*/
%include "../GRT/DataStructures/TimeSeriesClassificationData.h"
%include "../GRT/DataStructures/TimeSeriesClassificationSample.h"
%include "../GRT/DataStructures/UnlabelledData.h"
%include "../GRT/CoreModules/Classifier.h"
%include "../GRT/ClassificationModules/DTW/DTW.h"