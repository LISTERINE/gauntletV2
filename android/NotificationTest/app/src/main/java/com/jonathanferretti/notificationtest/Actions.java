package com.jonathanferretti.notificationtest;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

/**
 * Created by jon on 3/16/17.
 */

public class Actions {

    private NotificationManager mNotifyMgr;
    private static final long mVibratePattern[] = {0};
    private static Integer mNotificationId = 1000;
    public static final String CUSTOM_INTENT = "com.jferretti.action.TEST";

    public Actions(NotificationManager notificationManager){
        // Gets an instance of the NotificationManager service
        mNotifyMgr = notificationManager;
    }

    public void issueNotification(Context context, String body){
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setContentTitle("My notification")
                        .setSmallIcon(R.drawable.ic_android_black_18dp)
                        .setContentText(body)
                        .setVibrate(mVibratePattern);

        mNotificationId += 1;
        // Builds the notification and issues it.
        mNotifyMgr.notify(mNotificationId, mBuilder.build());
        mNotifyMgr.cancel(mNotificationId); // Remove notification immediately

        Intent i = new Intent();
        i.setAction(CUSTOM_INTENT);
        i.putExtra("gesture_name", body);
        context.sendBroadcast(i);
    }

}
