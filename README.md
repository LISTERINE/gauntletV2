Create a new gesture handler in Takser:
=======================================
1. Select the profiles tab at the top
2. Hit the plus sign in the lower right hand corner
3. Select "Event"->"UI"->"Notification"
4. Select "Owner Application" and change it to this app (this step is optional, but recommended)
5. In the "Text" field, enter the name of the gesture you are creating/have created ex. thumbs_up
6. If you have a task you want to accosicate, choose it from the list OR hit "New Task" (name it whatever you want)
7. Click plus to add an action, add as many actions as you want.

Create a gesture:
=================
1. Open the app
2. Select Adafruit Bluefruit LE from the list
3. Wait for the buttons at the top to become enabled. If they don't, hit the back button and select the device again. If it still won't load, completely kill the app and start it up again.
4. Hit the "Start Training" button, enter a name for the gesture. It must have the same name as the Tasker profile you made. To trigger the profile using the example above, the name should be "thumbs_up". Once you hit OK, the app will start recording the gloves actions. Once satisfied with the gesture you've made, hit the "Stop Recording" button (previously the Start Recording button).
5. If unsatisfied with the gesture you recoded, hit the gestures delete button and train again

Trigger a Tasker profile with a gesture you recorded:
=====================================================
1. Hit "Start Recording"
2. Perform a gesture that you previous trained
3. Hit "Stop Recording"

